﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NoktaApp.Models
{
    public class User
    {
        public int ID { get; set; }
        public string facebookId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Nokta> Nokat { get; set; }
     //   public virtual ICollection<Rank> Ranks { get; set; }

        public User()
        { }

        public User(MyAppUser user)
        {
            Name = user.Name;
            facebookId = user.Id;
        }

        public void DeleteNokat()
        {
            Nokat.Clear();
        }
    }
}