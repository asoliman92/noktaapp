﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Facebook;

namespace NoktaApp.Models
{
    public class Nokta
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public float? Rank { get; set; }
        public DateTime Date { get; set; }

        //relations
        public int UserID { get; set; }
        public int? LocationID { get; set; }
        public virtual User User { get; set; }
        public virtual Location Location { get; set; }
        public virtual ICollection<Rank> Ranks { get; set; }
        public Nokta(string title, string body)
        {
            // TODO: Complete member initialization
            this.Title = title;
            this.Body = body;
            Date = DateTime.Now;
            Rank = 0;
        }

        public Nokta(string title, string body, float longitude, float latitude)
        {
            // TODO: Complete member initialization
            this.Title = title;
            this.Body = body;
            Date = DateTime.Now;
            Location = new Location(longitude, latitude);
            LocationID = Location.ID;
            Rank = 0;
        }

        public Nokta()
        {
            Rank = 0;
            Ranks = new List<Rank>();
        }
    }
}