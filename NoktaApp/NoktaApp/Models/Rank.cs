﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NoktaApp.Models
{
    public class Rank
    {
        [Key, Column(Order = 0)]
        public int  UserId { get; set; }

        [Key, Column(Order = 1)]
        public int NoktaId { get; set; }

       public  float Score { get; set; }

        //public virtual User User { get; set; }
        public virtual Nokta Nokta { get; set; }

        public Rank()
        { }

        public Rank(int userId, Nokta nokta, float score)
        {
            UserId = userId;
            NoktaId = nokta.ID;
            Nokta = nokta;
            this.Score = score;            
        }
    }
}