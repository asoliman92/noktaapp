﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace NoktaApp.Models
{
    public class Location
    {
        public int ID { get; set; }
        public float? Longitude { get; set; }
        public float? Latitude { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Nokta> Nokat { get; set; }


        public Location()
        { 
        }
        public Location(float longitude, float latitude)
        {
            // TODO: Complete member initialization
            this.Longitude = longitude;
            this.Latitude = latitude;
        }
    }
}
