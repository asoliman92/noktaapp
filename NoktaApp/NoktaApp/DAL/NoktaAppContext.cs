﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using NoktaApp.Models;
using System.Data.Entity.Migrations;

namespace NoktaApp.DAL
{
    public class Configuration : DbMigrationsConfiguration<NoktaAppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }
    }
    public class NoktaAppContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Nokta> Nokat { get; set; }
        public DbSet<Rank> Ranks { get; set; }
        public DbSet<Location> Locations { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        //}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<NoktaAppContext, Configuration>());
        }

        public NoktaAppContext() : base("NoktaAppContext")
        {
           
        }

        public System.Data.Entity.DbSet<NoktaApp.Models.MyAppUser> MyAppUsers { get; set; }
    }
}


