﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Facebook;
using Microsoft.AspNet.Facebook.Client;
using NoktaApp.Models;
using System;
using NoktaApp.Helpers;
using NoktaApp.DAL;
using System.Data.Entity;

namespace NoktaApp.Controllers
{
    public class HomeController : Controller
    {

        private NoktaAppContext db = new NoktaAppContext();

        [FacebookAuthorize("email", "user_photos", "user_friends",  "publish_actions")]
        public async Task<ActionResult> Index(FacebookContext context)
        {
            if (ModelState.IsValid)
            {
               var fbUser = await context.Client.GetCurrentUserAsync<MyAppUser>();

               //adding user to DB if not there
               User user = db.Users.Where(m => m.facebookId == fbUser.Id).FirstOrDefault();
               if (user == null)
               {
                   user = new User();
                   user.facebookId = fbUser.Id;
                   user.Name = fbUser.Name;
                   user.Nokat = new List<Nokta>();
                   db.Users.Add(user);
                   db.SaveChanges();
               }

              

               //if (user.Nokat.Count == 0)
               //{
               //    //adding dummy nokat for testing

               //    user.Nokat = new List<Nokta>();
               //    for (int i = 0; i < 4; i++)
               //    {
               //        Nokta nokta = new Nokta();

               //        nokta.Title = "NoktaTitle " + i;
               //        nokta.Body = "This is " + i + " Nokta Body by " + user.Name + " ;)";
               //        nokta.Date = DateTime.Today;

               //        user.Nokat.Add(nokta);
                     
               //        db.SaveChanges();
               //    }
               //}

               // ////  for testing
               //if (user.Nokat.Count > 0)
               //{
               //    List<Nokta> nokat = db.Nokat.Where(m => m.UserID == user.ID).ToList();
               //    foreach (var nokta in nokat)
               //        db.Nokat.Remove(nokta);

               //    user.DeleteNokat();
               //    db.SaveChanges();
               //}

               Session["fbUser"] = fbUser;
               Session["UserID"] = user.ID;
              // return View(fbUser);
               return RedirectToAction("Details", "User", new { id = user.ID});
             
            }

            return View("Error");
        }

        // This action will handle the redirects from FacebookAuthorizeFilter when
        // the app doesn't have all the required permissions specified in the FacebookAuthorizeAttribute.
        // The path to this action is defined under appSettings (in Web.config) with the key 'Facebook:AuthorizationRedirectPath'.
        public ActionResult Permissions(FacebookRedirectContext context)
        {
            if (ModelState.IsValid)
            {
                return View(context);
            }

            return View("Error");
        }

        public ActionResult UserProfile()
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index", "User");
            }

            return View("Error");
        }
    }
}