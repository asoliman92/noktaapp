﻿using System;

using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NoktaApp.Models;
using NoktaApp.DAL;

namespace NoktaApp.Controllers
{
    public class NoktaComparer : IComparer<Nokta>
    {
        public int Compare(Nokta a, Nokta b)
        {
            if (a.Date == b.Date)
                return 0;
            if (a.Date > b.Date)
                return -1;

            return 1;
        }
    }

    public class UserController : Controller
    {
        private NoktaAppContext db = new NoktaAppContext();

        // GET: /User/
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }
        public ActionResult NokatFeed()
        {

            List<Nokta> friendsNokat = new List<Nokta>();
            MyAppUser fbUser = (MyAppUser)Session["fbUser"];
            User currentUser = db.Users.Where(m => m.facebookId == fbUser.Id).FirstOrDefault();

            List<MyAppUserFriend> friends = fbUser.Friends.Data.ToList();

            User tmpUser = new User();
            foreach (var friend in friends)
            {
                tmpUser = db.Users.Where(m => m.facebookId == friend.Id).FirstOrDefault();
                if(tmpUser != null)
                { 
                    foreach (var nokta in tmpUser.Nokat)
                        friendsNokat.Add(nokta);
                }
            }

            ViewBag.UserID = currentUser.ID;

            //sort them on date
            friendsNokat.Sort(new NoktaComparer());
           
            return View("NokatFeed", friendsNokat);
        }

        // GET: /User/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            User user = db.Users.Find(id);


            if (user == null)
            {
                return HttpNotFound();
            }

            ViewBag.fbUser = Session["fbUser"];

            return View(user);
        }


        //For reloading in <div> when posting new Nokta
        public PartialViewResult DisplayNokat(int id)
        {
            User user = db.Users.Find(id);
            List<Nokta> nokat = user.Nokat.ToList();
            return PartialView("NokatPartial", nokat);
        }


        //For Displaying Nokta in User page or NokatFeed
        public PartialViewResult NoktaPartial(int id)
        {   
            Nokta nokta = db.Nokat.Find(id);
            MyAppUser fbUser = (MyAppUser)Session["fbUser"];
            User user = db.Users.Where(m => m.facebookId == fbUser.Id).FirstOrDefault();
            Rank rank = db.Ranks.Where(m => m.NoktaId == id && m.UserId == user.ID).FirstOrDefault();
           
            if (rank != null)
                ViewBag.UserRank = rank.Score;
            else
                ViewBag.UserRank = 0;

            return PartialView("NoktaPartial", nokta);
        }


        [HttpPost]
        public void SetCurrentPosition(float latitude, float longitude)
        {
            @Session["currentLat"] = latitude;
            @Session["currentLng"] = longitude;
            return;
        }


        public ActionResult NoktaDetails(int id)
        {
            Nokta nokta = db.Nokat.Find(id);
            MyAppUser fbUser = (MyAppUser)Session["fbUser"];
            User user = db.Users.Where(m => m.facebookId == fbUser.Id).FirstOrDefault();
            Rank rank = db.Ranks.Where(m=> m.NoktaId == id && m.UserId == user.ID).FirstOrDefault();
            if (rank != null)
                ViewBag.UserRank = rank.Score;
            else
                ViewBag.UserRank = 0;

            return View("NoktaDetails", nokta);
        }

        public void PostNokta(string title, string body, float longitude, float latitude)
        {
            Nokta nokta = new Nokta(title, body, longitude, latitude);
            MyAppUser fbUser = (MyAppUser) Session["fbUser"];
            User user = db.Users.Where(m => m.facebookId == fbUser.Id).FirstOrDefault();
            user.Nokat.Add(nokta);
            db.SaveChanges();
        }

        public void RateNokta(float score, int noktaId)
        {
            MyAppUser fbUser = (MyAppUser)Session["fbUser"];
          
            User user = db.Users.Where(m => m.facebookId == fbUser.Id).FirstOrDefault();
            Rank rank = db.Ranks.Where(m => m.NoktaId == noktaId && m.UserId == user.ID).FirstOrDefault();
            Nokta nokta = db.Nokat.Where(m => m.ID == noktaId).FirstOrDefault();

            if (rank == null)
            {
                //add this rank
                rank = new Rank(user.ID, nokta, score);

                //calculate the avg rank of nokta
                //1- get count of all users rated it from ranks excluding current user (just rated it)
                float count = db.Ranks.Where(m => m.NoktaId == noktaId).Count();
                //2- add new score and divide by count+1
                nokta.Rank = 0;
                nokta.Rank = (nokta.Rank + score) / (count + 1);

                if (nokta.Ranks == null)
                 nokta.Ranks = new List<Rank>();

                nokta.Ranks.Add(rank);
                db.SaveChanges();
            }
            else
            { 
                //user rated this nokta before but changed his mind
                float prevRank = rank.Score;
                rank.Score = score; // new score
                //1- get count of all users rated it from ranks including current user
                float count = db.Ranks.Where(m => m.NoktaId == noktaId).Count();
                float sum = db.Ranks.Where(m => m.NoktaId == noktaId).Sum(z => z.Score);

                nokta.Rank = (sum - prevRank + score) / count;
                db.SaveChanges();
            }

        }
     

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}